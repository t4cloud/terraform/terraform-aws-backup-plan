output "plan_id" {
  value       = "${aws_backup_plan.default.id}"
  description = "The ID of backup plan"
}

output "vault" {
  value       = "${var.name}"
  description = "The name of vault"
}

output "role_arn" {
  value       = "${element(concat(aws_iam_role.default.*.arn, list("")), 0)}"
  description = "ARN of IAM role"
}
